import { BoatControl, RoutingMethod } from "./BoatControl/BoatControl";

/**
 * The contest rules did not say anything about the red and green guides
 * that are visible on the lake.
 * I assumed they are sea marks that guide the ships on the lake.
 * 
 * Therefore the BoatControl can be instantiated in two different modes:
 * * GUIDED_ROUTE: The boat respectes the sea marks and travels only between 
 *   their boundaries.
 * 
 * * DIRECT_ROUTE: The boat ignores the red and green guides and travels on 
 *   a direct route to the target.
 * 
 */

let boatControl: BoatControl = new BoatControl(RoutingMethod.GUIDED_ROUTE);
boatControl.init();