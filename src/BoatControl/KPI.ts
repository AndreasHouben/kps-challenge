export class KPI  {
    constructor(private element: HTMLSpanElement) {
    }

    setText = (text: string) => {
        this.element.innerText = text;
    }

}