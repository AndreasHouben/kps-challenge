import { Coordinate } from "./Coordinate";

export class Lake {

    constructor(private element: HTMLElement) {
    }

    getCenter = (): Coordinate => {
        return new Coordinate(
            this.element.clientWidth / 2,
            this.element.clientHeight / 2
        );
    }


}