import { Coordinate } from "./Coordinate";

export class Station {

    private number: number;

    constructor(private element: HTMLDivElement) {        
        this.number = Number.parseInt((element.firstChild as HTMLObjectElement).data);        
    }

    getLocation = (): Coordinate => {
        return  new Coordinate(
            this.element.offsetLeft ,
            this.element.offsetTop,
            `Station ${this.number}`
        );    
    }


}