export class Coordinate {

    constructor(public x: number, public y: number, public name?: string) {
    }

    isEqualTo = (otherCoordinate: Coordinate): boolean => {
        return otherCoordinate.x === this.x && otherCoordinate.y == this.y;
    }
    
    gradientTo = (otherCoordinate: Coordinate): number => {
        return (otherCoordinate.y - this.y) / (otherCoordinate.x - this.x);
    }
}
