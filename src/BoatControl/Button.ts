export class Button {

    constructor(private element: HTMLButtonElement) {
    }

    public setOnClick = (onClick: () => void) => {
        this.element.onclick = () => onClick();
    }

}
